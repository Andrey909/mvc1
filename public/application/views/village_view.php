    <!DOCTYPE html>
    <html>

    <meta charset="utf-8">
    <meta http-equiv="Conten-Type" content="text/html;>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Село</title>
    <meta name="description" content="верстка">
    <meta name="keywords" content="мета-теги, поисковые системы, ключевые слова">
    <link rel="stylesheet" type="text/css" href="/../../css/stylesite.css">

    </head>
<body>
<?php
if(!isset ($_GET['page'])) {$_GET['page'] = 'main';}
?>
<div id="wrapper">
    <div class="header">
        <div class="logo left">
            <a href="#"><img src="/../../image/Yasky.png" height="120px" width="90px"></a>
            <a href="#"><img src="/../../image/neo2.png"></a>
        </div>
        <div class="poisk"> <form>
                <p><input type="search" name="q" placeholder="Поиск по сайту">
                    <input type="submit" value="Найти"></p>
            </form></div>
        <div class="nav">
            <ul>
                <li><a href="yasky.php?page=main">Головна</a></li>
                <li><a href="yasky.php?page=history">Історія села</a></li>
                <li><a href="yasky.php?page=our">Наше село</a></li>
                <li><a href="yasky.php?page=gallery">Галерея</a></li>
                <li><a href="yasky.php?page=contacts">Контакти</a></li>
            </ul>
        </div>
    </div>
    <div id="reka">
    </div>
    <div class="content clear">
        <div class="content1">

            <h1> Cела Яськи</h1>
            <p>Яськи - село, центр сільської ради. Розташоване на вiдстанi 60 км від м.Одеси. Село розкинулось у мальовничій рівнинній місцевості, вздовж лівого берега річки Турунчука (рукав Дністра), поблизу озер та плавнів. </p>

            <div class="karta">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d43930.53703538363!2d30.04916500341759!3d46.51486860645177!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2sua!4v1499116345555" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>

        </div>
        <div class="content2">
            <div class="info">
                <h3 class="infotext">
                    <span>ОСНОВНІ ДАННІ</span>
                </h3>                                                                     <p> Засноване	1780 р.</p>
                <p> Населення	4145 чоловік</p>
                <p> Площа	8,52 км²</p>
                <p> Густота населення	486,5 осіб/км²</p>
                <p> Поштовий індекс	67642</p>
                <p> Телефонний код	+380 4852</p>
                <p> Географічні координати	46°30′54″ пн. ш. 30°05′03″ сх. д.</p>
                <p>Середня висота над рівнем моря	2 м</p>
                <p>Водойми	р. Турунчук, озера Писарське, Тудорове, Чорне </p>
            </div>
            <div class="pogoda">
                <h3 class="pogodatext">
                    <span>ПОГОДА</span>
                </h3>
                <div class="gis">
                    <!-- Gismeteo informer START -->
                    <link rel="stylesheet" type="text/css" href="https://s1.gismeteo.ua/static/css/informer2/gs_informerClient.min.css">
                    <div id="gsInformerID-pb7ISf6P5fF4yh" class="gsInformer" style="width:230px;height:225px">
                        <div class="gsIContent">
                            <div id="cityLink">
                                <a href="https://www.gismeteo.ua/ua/weather-yasky-90319/" title="Погода в Яськах" target="_blank">
                                    <img src="https://s1.gismeteo.ua/static/images/gisloader.svg" width="24" height="24" alt="Погода в Яськах">
                                </a>
                            </div>
                            <div class="gsLinks">
                                <table>
                                    <tr>
                                        <td>
                                            <div class="leftCol">
                                                <a href="https://www.gismeteo.ua/ua/" target="_blank" title="Погода">
                                                    <img alt="Погода" src="https://s1.gismeteo.ua/static/images/informer2/logo-mini2.png" align="middle" border="0" height="16" width="11"/>
                                                    <img src="https://s1.gismeteo.ua/static/images/gismeteo.svg" border="0" align="middle" style="left: 5px; top:1px">
                                                </a>
                                            </div>
                                            <div class="rightCol">
                                                <a href="https://www.gismeteo.ua/ua/weather-yasky-90319/14-days/" target="_blank" title="Погода в Яськах на 2 тижні">
                                                    <img src="https://s1.gismeteo.ua/static/images/informer2/forecast-2weeks.ua.svg" border="0" align="middle" style="top:auto" alt="Погода в Яськах на 2 тижні">
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <script src="https://www.gismeteo.ua/ajax/getInformer/?hash=pb7ISf6P5fF4yh" type="text/javascript"></script>
                    <!-- Gismeteo informer END -->
                </div>
            </div>
            <div class="video">
                <h3 class="videotext">
                    <span>ВІДЕО</span>
                </h3>
                <div class="vid">
                    <object type="application/x-shockwave-flash" id="video0" name="video0" data="http://glasweb.com/wp-content/plugins/flash-video-player/mediaplayer/player.swf" width="340" height="315" style="visibility: visible;"><param name="allowfullscreen" value="true"><param name="allowscriptaccess" value="always"><param name="wmode" value="transparent"><param name="flashvars" value="file=http://glasweb.com/wp-content/uploads/2010/09/istoki_11_panenko_ss_26_04_05_07.flv&amp;image=http://glasweb.com/wp-content/uploads/2010/09/Still0906_000011.jpg&amp;provider=video&amp;width=600&amp;height=400&amp;controlbar=bottom&amp;dock=false&amp;icons=true&amp;logo.hide=false&amp;logo.position=bottom-left&amp;playlist=none&amp;autostart=false&amp;bufferlength=1&amp;item=0&amp;mute=false&amp;repeat=none&amp;shuffle=false&amp;smoothing=true&amp;stretching=fill&amp;volume=90"></object>
                </div>
            </div>
        </div>
    </div>
    <div class="clear">
    </div>
    <div class="lin">
    </div>
    <div class="footer">
        © 2018 Яськи - неофіційний сайт села.
    </div>
</div>

</body>
</html>