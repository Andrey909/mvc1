<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
</head>
<body>

<div class="jumbotron text-center">
    <h1>My First MVC</h1>
    <ul>
        <li> <a href="/"> Main page</a> </li>
        <li> <a href="/Main/Contacts"> Contacts page</a> </li>
        <li> <a href="/Portfolio/index">Portfolio</a> </li>
        <li> <a href="/Articles/index">Articles</a> </li>
        <li> <a href="/Blog/index">Blog main page</a> </li>
        <li> <a href="/Blog/Category">Blog Category page</a> </li>

    </ul>
</div>


<div id="content">
    <?php require 'application/views/'.$contentView.'_view.php'?>
</div>


<div id="footer">
    (c) Hillel MVC Project
</div>
</body>
</html>