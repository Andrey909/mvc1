<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <h3>Проект №1</h3>
            <p> Реализован в <?=$data['allData'][0]['year']?> году</p>
            <p><?=$data['allData'][0]['description']?></p>
            <p><a href=<?=$data['allData'][0]['url']?>>Подробнее...</a></p>
        </div>
        <div class="col-sm-4">
            <h3>Проект №2</h3>
            <p>Реализован в <?=$data['allData'][1]['year']?> году</p>
            <p><?=$data['allData'][1]['description']?></p>
            <p><a href=<?=$data['allData'][1]['url']?>>Подробнее...</a></p>
        </div>
        <div class="col-sm-4">
            <h3>Проект №3</h3>
            <p>Реализован в <?=$data['allData'][2]['year']?> году</p>
            <p><?=$data['allData'][2]['description']?></p>
            <p><a href=<?=$data['allData'][2]['url']?>>Подробнее...</a></p>
        </div>
    </div>
</div>
