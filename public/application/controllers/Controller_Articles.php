<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 07.01.2018
 * Time: 19:53
 */

class Controller_Articles extends Controller
{
    public $model;

    public function __construct()
    {
        parent::__construct();
        require 'application/models/Model_Articles.php';
        $this->model = new Model_Articles();
    }
    public function action_index(){
        $data['articlesData'] = $this->model->getbd();
        $this->view->generate('articles', $data);
    }
}