<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 07.01.2018
 * Time: 14:48
 */

class Controller_Portfolio extends Controller
{
    public $model;

    public function __construct()
    {
        parent::__construct();
        require 'application/models/Model_Portfolio.php';
        $this->model = new Model_Portfolio();
    }
    public function action_index($params)
    {
        if (!empty($params[0])) {
            if ($params[0] == 'village') {
                require 'application/views/village_view.php';
                die();
            } else if ($params[0] == 'first') {
                require 'application/views/first_view.php';
                die();
            } else if ($params[0] == 'second') {
                require 'application/views/second_view.php';
                die();
            } else {
                Route::errorPage404();
            }
        }
        $data['allData'] = $this->model->getbd();
        $this->view->generate('portfolio', $data);
    }
}
